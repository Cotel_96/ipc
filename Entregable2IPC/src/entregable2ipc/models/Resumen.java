/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregable2ipc.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Cotel
 */
public class Resumen implements Serializable, Comparable {
    
    private Date fecha;
    private long tiempo;
    private double distancia;

    public Resumen(Date fecha, long tiempo, double distancia) {
        this.fecha = fecha;
        this.tiempo = tiempo;
        this.distancia = distancia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Resumen) {
            Resumen aux = (Resumen) o;
            if(this.fecha.before(aux.getFecha())) {
                return -1;
            } else if(this.fecha.after(aux.getFecha())) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    public boolean equals(Object o) {
        if(o instanceof Resumen) {
            Resumen aux = (Resumen) o;
            if(this.fecha == aux.getFecha() &&
               this.tiempo == aux.getTiempo() &&
               this.distancia == aux.getDistancia()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
}
