/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregable2ipc.models;

/**
 *
 * @author Cotel
 */
public class Zona {
    
    private int contador;
    
    public Zona() {
        this.contador = 0;
    }
    
    public void addOne() {
        this.contador++;
    }
    
    public int getContador() {
        return this.contador;
    }
    
}
