/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregable2ipc.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;
import entregable2ipc.MainApp;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import jgpx.model.jaxb.GpxType;
import jgpx.model.jaxb.TrackPointExtensionT;

/**
 * FXML Controller class
 *
 * @author djcot
 */
public class PreloaderViewController implements Initializable {

    @FXML private JFXButton anadirArchivo;
    @FXML private JFXSpinner progress;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarArchivo(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir .gpx");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Archivo GPX", "*.gpx")
        );
        
        File file = fileChooser.showOpenDialog(anadirArchivo.getScene().getWindow());
        if(file != null) {
            try {
                anadirArchivo.setText("Cargando...");
                anadirArchivo.getScene().setCursor(Cursor.WAIT);
                progress.setVisible(true);
                FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("views/MainView.fxml"));
                Parent viewRoot = loader.load();
                MainViewController controller = loader.<MainViewController>getController();
                
                JAXBContext jaxbContext = JAXBContext.newInstance(GpxType.class, TrackPointExtensionT.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(file);
                GpxType gpx = (GpxType) root.getValue();                
                
                if(gpx != null) {
                    Task<Void> loadView = new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            controller.setRuta(gpx);                        
                            return null;
                        }

                        @Override
                        protected void succeeded() {
                            Stage stage = new Stage();
                            Scene scene = new Scene(viewRoot);
                            stage.setScene(scene);
                            anadirArchivo.getScene().setCursor(Cursor.DEFAULT);
                            anadirArchivo.getScene().getWindow().hide();
                            
                            controller.setWindow(stage);

                            stage.setMinWidth(621);
                            stage.setMinHeight(775);
                            stage.show();
                        }
                    };

                    Thread th = new Thread(loadView);
                    th.setDaemon(true);
                    th.start();
                }
                
                
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Hubo un error!");
                alert.setContentText("Error durante la lectura del archivo gpx");
                alert.showAndWait();
                System.exit(0);
            }
        }
    }
    
}
