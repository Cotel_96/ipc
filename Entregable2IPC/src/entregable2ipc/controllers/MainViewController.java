/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregable2ipc.controllers;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXToggleButton;
import entregable2ipc.MainApp;
import entregable2ipc.models.Resumen;
import entregable2ipc.models.Zona;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import jgpx.model.analysis.Chunk;
import jgpx.model.analysis.TrackData;
import jgpx.model.gpx.Track;
import jgpx.model.jaxb.GpxType;

/**
 * FXML Controller class
 *
 * @author djcot
 */
public class MainViewController implements Initializable {

    @FXML
    private Label inicioValue;
    @FXML
    private Label durValue;
    @FXML
    private Label tmValue;
    @FXML
    private Label dtValue;
    @FXML
    private Label DesniSubValue;
    @FXML
    private Label DesniBajValue;
    @FXML
    private Label VelMaxValue;
    @FXML
    private Label VelMedValue;
    @FXML
    private Label FCMinValue;
    @FXML
    private Label FCMedValue;
    @FXML
    private Label FCMaxValue;
    @FXML
    private Label CDMaxValue;
    @FXML
    private Label CDMedValue;
    @FXML
    private AreaChart<Number, Number> AltxDist;
    @FXML
    private LineChart<Number, Number> VelxDist;
    @FXML
    private BarChart<String, Number> resumenChart;
    @FXML
    private NumberAxis AreaxAxis;
    @FXML
    private NumberAxis LinexAxis;
    @FXML
    private PieChart zonasPie;
    @FXML
    private JFXCheckBox velEnabled;
    @FXML
    private JFXCheckBox fcEnabled;
    @FXML
    private JFXCheckBox cadEnabled;
    @FXML
    private JFXToggleButton toggleVel;
    @FXML
    private JFXHamburger optionsButton;
    @FXML
    private JFXSpinner progressArea;
    @FXML
    private JFXSpinner progressLine;
    
    private Stage thiswindow;
    
    private GpxType ruta;
    private TrackData trackData;
    
    private XYChart.Series AltDistSer;
    private XYChart.Series VelDistSer;
    private XYChart.Series FCDistSer;
    private XYChart.Series CADDistSer;
    private XYChart.Series AltTempSer;
    private XYChart.Series VelTempSer;
    private XYChart.Series FCTempSer;
    private XYChart.Series CADTempSer;
    private XYChart.Series serieD;
    private XYChart.Series serieT;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AltxDist.setCreateSymbols(false);
        VelxDist.setCreateSymbols(false);
        AltxDist.setAnimated(false);
        VelxDist.setAnimated(false);
        
        velEnabled.setSelected(true);
        fcEnabled.setSelected(true);
        cadEnabled.setSelected(true);
        
        serieD = new XYChart.Series();
        serieD.setName("Distancia (km)");
        serieT = new XYChart.Series();
        serieT.setName("Tiempo (m)");
        
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Date now = new Date();
                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
                    File file = new File("data.db");
                    FileInputStream fis = new FileInputStream(file);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    
                    TreeSet<Resumen> res = (TreeSet<Resumen>) ois.readObject();
                    
                    ois.close();
                    
                    for (Resumen r : res) {
                        if(r.getFecha().getMonth() >= now.getMonth()-3) {
                            serieD.getData().add(new XYChart.Data(dt.format(r.getFecha()), r.getDistancia()/1000));
                            serieT.getData().add(new XYChart.Data(dt.format(r.getFecha()), r.getTiempo()));
                        }
                    }
                    
                    resumenChart.getData().addAll(serieD, serieT);
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
             
                return null;
            }
            
        };
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
        
    }
    
    public void setWindow(Stage stage) {
        this.thiswindow = stage;
    }
    
    public void setRuta(GpxType t) {
        this.ruta = t;
        this.trackData = new TrackData(new Track(ruta.getTrk().get(0)));
        ObservableList<Chunk> chunks = trackData.getChunks();
        
        Task<Void> taskResumen = new Task<Void>() {
            @Override
            protected Void call() throws Exception {                
                fillResumen(trackData);
                return null;
            }            
        };
        Thread resumenThread = new Thread(taskResumen);
        resumenThread.setDaemon(true);
        resumenThread.start();
        
        Task<Void> task0 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                AltDistSer = fillGrafAreas(chunks);
                return null;
            }
            
            @Override
            protected void succeeded() {
                super.succeeded();
                AltxDist.getData().addAll(AltDistSer);
                progressArea.setVisible(false);
            }
        };
        Thread th0 = new Thread(task0);
        th0.setDaemon(true);
        th0.start();
        
        Task<Void> task1 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                CADDistSer = fillGrafCad(chunks);
                return null;
            }
            
            protected void succeeded() {
                super.succeeded();
                CADDistSer.setName("Cadencia (pdas/min)");
                VelxDist.getData().add(CADDistSer);
                progressLine.setVisible(false);
                CADDistSer.getNode().visibleProperty().bind(cadEnabled.selectedProperty());
            }
        };
        Thread th1 = new Thread(task1);
        th1.setDaemon(true);
        th1.start();
        
        Task<Void> task2 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                FCDistSer = fillGrafFC(chunks);
                return null;
            }
            
            @Override
            protected void succeeded() {
                super.succeeded();
                FCDistSer.setName("Frec. Cardiaca (puls/min)");
                VelxDist.getData().add(FCDistSer);
                progressLine.setVisible(false);
                FCDistSer.getNode().visibleProperty().bind(fcEnabled.selectedProperty());
            }
        };
        Thread th2 = new Thread(task2);
        th2.setDaemon(true);
        th2.start();
        
        Task<Void> task3 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                VelDistSer = fillGrafVel(chunks);
                return null;
            }
            
            @Override
            protected void succeeded() {
                super.succeeded();
                VelDistSer.setName("Velocidad (Km/h)");
                VelxDist.getData().add(VelDistSer);
                progressLine.setVisible(false);
                VelDistSer.getNode().visibleProperty().bind(velEnabled.selectedProperty());
            }
        };
        Thread th3 = new Thread(task3);
        th3.setDaemon(true);
        th3.start();
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                zonasPie.setData(fillPie(chunks, trackData.getMaxHeartrate()));
            }            
        });
                
    }
    
    @FXML
    public void newGPX() {
        System.out.println("Clicked");
        try {
            Parent root = FXMLLoader.load(MainApp.class.getResource("views/PreloaderView.fxml"));
            
            Scene scene = new Scene(root);
            
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            this.thiswindow.hide();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addToResumen() {
        Calendar cal = Calendar.getInstance();
        cal.set(trackData.getStartTime().getYear(),
                trackData.getStartTime().getMonthValue()-1,
                trackData.getStartTime().getDayOfMonth());
        
        Resumen res = new Resumen(cal.getTime(),
                trackData.getTotalDuration().toMinutes(),
                trackData.getTotalDistance());
        
        try {
            TreeSet<Resumen> Lres;
            File file = new File("data.db");
            if(file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);

                Lres = (TreeSet<Resumen>) ois.readObject();
                ois.close();
                
                if(!Lres.contains(res)) {
                    Lres.add(res);
                }
            } else {
                Lres = new TreeSet<Resumen>();
                Lres.add(res);
            }
            
            if(!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(Lres);
            oos.close();
            
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
                    Date now = new Date();
                    serieT.getData().clear();
                    serieD.getData().clear();
                    for(Resumen r : Lres) {
                        if(r.getFecha().getMonth() >= now.getMonth()-3) {
                            serieD.getData().add(new XYChart.Data(dt.format(r.getFecha()), r.getDistancia()/1000));
                            serieT.getData().add(new XYChart.Data(dt.format(r.getFecha()), r.getTiempo()));
                        }
                    }
                    resumenChart.getData().setAll(serieD, serieT);
                }        
            });


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
    }
    
    public void fillResumen(TrackData trackData) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy\nHH:mm:ss");
        LocalTime durTotal = LocalTime.ofNanoOfDay(trackData.getTotalDuration().toNanos());
        LocalTime tmTotal = LocalTime.ofNanoOfDay(trackData.getMovingTime().toNanos());

        inicioValue.setText(trackData.getStartTime().format(dtf));
        durValue.setText(durTotal.toString());
        tmValue.setText(tmTotal.toString());

        String aux = String.format("%.2f Km", trackData.getTotalDistance()/1000);
        dtValue.setText(aux);

        aux = String.format("Subida: %.2f m", trackData.getTotalAscent());
        DesniSubValue.setText(aux);
        aux = String.format("Bajada: %.2f m", trackData.getTotalDescend());
        DesniBajValue.setText(aux);

        aux = String.format("Max: %.2f Km/h", trackData.getMaxSpeed());
        VelMaxValue.setText(aux);
        aux = String.format("Media: %.2f Km/h", trackData.getAverageSpeed());
        VelMedValue.setText(aux);

        aux = String.format("Max: %d puls/min", trackData.getMaxHeartrate());
        FCMaxValue.setText(aux);
        aux = String.format("Med: %d puls/min", trackData.getAverageHeartrate());
        FCMedValue.setText(aux);
        aux = String.format("Min: %d puls/min", trackData.getMinHeartRate());
        FCMinValue.setText(aux);

        aux = String.format("Max: %d pdas/min", trackData.getMaxCadence());
        CDMaxValue.setText(aux);
        aux = String.format("Media: %d pdas/min", trackData.getAverageCadence());
        CDMedValue.setText(aux);
        
    }
    
    private XYChart.Series fillGrafAreas(ObservableList<Chunk> chunks) {
        XYChart.Series res = new XYChart.Series();
        XYChart.Series restemp = new XYChart.Series();
        
        double duration = 0;
        double distance = 0;
        for(Chunk chunk : chunks) {
            res.getData().add(new XYChart.Data(distance/1000, chunk.getLastPoint().getElevation()));
            restemp.getData().add(new XYChart.Data(duration/60, chunk.getLastPoint().getElevation()));
            distance += chunk.getDistance();
            duration += chunk.getDuration().getSeconds();
        }
        
        AltTempSer = restemp;
        return res;        
    }
    
    private XYChart.Series fillGrafVel(ObservableList<Chunk> chunks) {
        XYChart.Series res = new XYChart.Series();
        XYChart.Series restemp = new XYChart.Series();
        
        double duration = 0;
        double distance = 0;
        for(Chunk chunk : chunks) {
            res.getData().add(new XYChart.Data(distance/1000, chunk.getSpeed()));
            restemp.getData().add(new XYChart.Data(duration/60, chunk.getSpeed()));
            distance += chunk.getDistance();
            duration += chunk.getDuration().getSeconds();
        }
        
        VelTempSer = restemp;
        VelTempSer.setName("Velocidad (Km/h)");
        return res; 
    }
    
    private XYChart.Series fillGrafFC(ObservableList<Chunk> chunks) {
        XYChart.Series res = new XYChart.Series();
        XYChart.Series restemp = new XYChart.Series();
        
        double duration = 0;
        double distance = 0;
        for(Chunk chunk : chunks) {
            res.getData().add(new XYChart.Data(distance/1000, chunk.getAvgHeartRate()));
            restemp.getData().add(new XYChart.Data(duration/60, chunk.getAvgHeartRate()));
            distance += chunk.getDistance();
            duration += chunk.getDuration().getSeconds();
        }
        
        FCTempSer = restemp;
        FCTempSer.setName("Frec. Cardiaca (puls/min)");
        return res; 
    }
    
    private XYChart.Series fillGrafCad(ObservableList<Chunk> chunks) {
        XYChart.Series res = new XYChart.Series();
        XYChart.Series restemp = new XYChart.Series();
        
        double duration = 0;
        double distance = 0;
        for(Chunk chunk : chunks) {
            res.getData().add(new XYChart.Data(distance/1000, chunk.getAvgCadence()));
            restemp.getData().add(new XYChart.Data(duration/60, chunk.getAvgCadence()));
            distance += chunk.getDistance();
            duration +=  chunk.getDuration().getSeconds();
        }
        
        CADTempSer = restemp;
        CADTempSer.setName("Cadencia (pdas/min)");
        return res; 
    }
    
    @FXML
    private void changeXAxis(ActionEvent event) {
        if(toggleVel.isSelected()) {
            toggleVel.setText("Tiempo");
            AreaxAxis.setLabel("Tiempo (minutos)");
            LinexAxis.setLabel("Tiempo (minutos)");
            
            AltxDist.getData().setAll(AltTempSer);
            VelxDist.getData().setAll(CADTempSer, FCTempSer, VelTempSer);
            
            CADTempSer.getNode().visibleProperty().bind(cadEnabled.selectedProperty());
            FCTempSer.getNode().visibleProperty().bind(fcEnabled.selectedProperty());
            VelTempSer.getNode().visibleProperty().bind(velEnabled.selectedProperty());
            
        } else {
            toggleVel.setText("Distancia");
            AreaxAxis.setLabel("Distancia (km)");
            LinexAxis.setLabel("Distancia (km)");
            
            AltxDist.getData().setAll(AltDistSer);
            VelxDist.getData().setAll(CADDistSer, FCDistSer, VelDistSer);
            
            CADDistSer.getNode().visibleProperty().bind(cadEnabled.selectedProperty());
            FCDistSer.getNode().visibleProperty().bind(fcEnabled.selectedProperty());
            VelDistSer.getNode().visibleProperty().bind(velEnabled.selectedProperty());
            
        }
    }
    
    private ObservableList<PieChart.Data> fillPie(ObservableList<Chunk> chunks, int maxFC) {
        ObservableList<PieChart.Data> res = FXCollections.observableArrayList();
        
        Zona z1 = new Zona();
        Zona z2 = new Zona();
        Zona z3 = new Zona();
        Zona z4 = new Zona();
        Zona z5 = new Zona();
        
        for(Chunk chunk : chunks) {
            if(chunk.getAvgHeartRate() < percentage(60.0, maxFC)) {
                z1.addOne();
            } else if(chunk.getAvgHeartRate() < percentage(70.0, maxFC)) {
                z2.addOne();
            } else if(chunk.getAvgHeartRate() < percentage(80.0, maxFC)) {
                z3.addOne();
            } else if(chunk.getAvgHeartRate() < percentage(90.0, maxFC)) {
                z4.addOne();
            } else {
                z5.addOne();
            }
        }
        
        res.add(new PieChart.Data("Z1 Recuperacion", z1.getContador()));
        res.add(new PieChart.Data("Z2 Fondo", z2.getContador()));
        res.add(new PieChart.Data("Z3 Tempo", z3.getContador()));
        res.add(new PieChart.Data("Z4 Umbral", z4.getContador()));
        res.add(new PieChart.Data("Z5 Anaerobico", z5.getContador()));
        
        return res;
        
    }
    
    private int percentage(double perc, double of) {
        return (int) (of * perc/100.0);
    }
        
}
