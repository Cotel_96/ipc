/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author micomuo
 */
public class FXMLDocumentController {
    
    @FXML 
    private Label resultado;
    @FXML 
    private Label advertencia;
    @FXML
    private CheckBox restaActivada;
    @FXML
    private Button uno;
    @FXML
    private Button cinco;
    @FXML
    private Button diez;
    @FXML
    private Button suma;
    @FXML
    private TextField input;
    
    private boolean resta;
    
    public void initialize() {
        resultado.setText("0.0");
        advertencia.setVisible(false);
        restaActivada.setOnAction(new EventHandler<ActionEvent> (){
            @Override
            public void handle(ActionEvent event) {
                if(restaActivada.isSelected()) {
                    resta = true;
                    advertencia.setVisible(true);
                } else {
                    resta = false;
                    advertencia.setVisible(false);
                }
            }
        });
    }

    public void operacionFija(ActionEvent event){
        double aux = Double.parseDouble(resultado.getText());
        Button buttonClicked = (Button) event.getSource();
        if(!resta) {
            if(buttonClicked.getId().equals("uno")) {
                aux += 1;
            } else if(buttonClicked.getId().equals("cinco")) {
                aux += 5;
            } else {
                aux += 10;
            }
        } else {
            if(buttonClicked.getId().equals("uno")) {
                aux -= 1;
            } else if(buttonClicked.getId().equals("cinco")) {
                aux -= 5;
            } else {
                aux -= 10;
            }
        }
        resultado.setText(Double.toString(aux));
    }
    
    public void operacionVariable()  {
        double aux = Double.parseDouble(resultado.getText());
        double cantidad = 0;
        if(!input.getText().equals("")) {
            cantidad = Double.parseDouble(input.getText());
        }
        if(!resta) {
            aux += cantidad;
        } else {
            aux -= cantidad;
        }
        resultado.setText(Double.toString(aux));
    }
    
}
