/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personmanager.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import personmanager.PersonManagerApp;
import personmanager.model.Person;

/**
 * FXML Controller class
 *
 * @author mario
 */
public class PersonManagerViewController {

    @FXML
    private TableView<Person> tableView;
    @FXML
    private TableColumn<Person, String> firstNameColumn;
    @FXML
    private TableColumn<Person, String> lastNameColumn;
    @FXML
    private TableColumn<Person, String> imageColumn;
    @FXML
    private Button buttonAdd;
    @FXML
    private Button buttonRemove;
    @FXML
    private Button buttonEdit;
    @FXML
    private TextField firstNameText;
    @FXML
    private TextField lastNameText;

    private ObservableList<Person> data = null;

    public void initialize() {        
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("lastName"));
        imageColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPathImage()));
        imageColumn.setCellFactory(c -> {
            return new TableCell<Person, String>() {
                private ImageView view = new ImageView();
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item == null || empty) setGraphic(null);
                    else {
                        Image imageAux = new Image(PersonManagerApp.class.
                            getResourceAsStream(item), 40, 40, true, true);
                        view.setImage(imageAux);
                        setGraphic(view);
                    }
                }
            };
        });
        tableView.setItems(data);
        final BooleanBinding noPersonSelected = Bindings.isNull(
                tableView.getSelectionModel().selectedItemProperty());
//        final BooleanBinding invalidName = Bindings.isEmpty(firstNameText.textProperty())
//                .or(Bindings.isEmpty(lastNameText.textProperty()));
        buttonRemove.disableProperty().bind(noPersonSelected);
        buttonEdit.disableProperty().bind(noPersonSelected);

    }
    
    public void initData(ObservableList data) {
        this.data = data;
        tableView.setItems(data);
    }
    
    public ObservableList getPersons() {
        return data;
    }
    
    @FXML
    private void onAddItem(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(PersonManagerApp.class.getResource("view/PersonDetailsView.fxml"));
        try {
            VBox root = (VBox) loader.load();
            PersonDetailsViewController controller = loader.<PersonDetailsViewController>getController();
            controller.initPersona(null, -1);
            controller.initData(data);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("New Person");
            stage.setResizable(false);
            controller.initStage(stage);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PersonManagerViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onEditItem(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(PersonManagerApp.class.getResource("view/PersonDetailsView.fxml"));
        try {
            VBox root = (VBox) loader.load();
            PersonDetailsViewController controller = loader.<PersonDetailsViewController>getController();
            controller.initPersona(tableView.getSelectionModel().getSelectedItem(), tableView.getSelectionModel().getSelectedIndex());
            controller.initData(data);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("New Person");
            stage.setResizable(false);
            controller.initStage(stage);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PersonManagerViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onRemoveItem(ActionEvent event) {
        data.remove(tableView.getSelectionModel().getSelectedIndex());
    }

}
