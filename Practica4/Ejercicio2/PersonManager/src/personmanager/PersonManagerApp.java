/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personmanager;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import personmanager.model.Person;
import personmanager.controller.PersonManagerViewController;

/**
 *
 * @author mario
 */
public class PersonManagerApp extends Application {
    
    private PersonManagerViewController controller;
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/personmanager/view/PersonManagerView.fxml"));
        Parent root = loader.load();
        controller = loader.<PersonManagerViewController>getController();
        ArrayList<Person> backupList = new ArrayList<>();
        backupList.add(new Person("John", "Doe"));
        backupList.add(new Person("Jane", "Doe"));
        ObservableList<Person> data = FXCollections.observableArrayList(backupList);
        controller.initData(data);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("ListView example");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        launch(args);
    }
    
}
