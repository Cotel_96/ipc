/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personmanager.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import personmanager.PersonManagerApp;
import personmanager.model.Person;

/**
 * FXML Controller class
 *
 * @author mario
 */
public class PersonManagerViewController {

    @FXML
    private ListView<Person> listView;
    @FXML
    private Button buttonAdd;
    @FXML
    private Button buttonRemove;
    @FXML
    private Button buttonEdit;
    @FXML
    private TextField firstNameText;
    @FXML
    private TextField lastNameText;

    private ObservableList<Person> data = null;

    public void initialize() {
        ArrayList<Person> backupList = new ArrayList<>();
        backupList.add(new Person("John", "Doe"));
        backupList.add(new Person("Jane", "Doe"));
        data = FXCollections.observableArrayList(backupList);
        listView.setItems(data);
        listView.setCellFactory(c -> new PersonListCell());
        final BooleanBinding noPersonSelected = Bindings.isNull(
                listView.getSelectionModel().selectedItemProperty());
//        final BooleanBinding invalidName = Bindings.isEmpty(firstNameText.textProperty())
//                .or(Bindings.isEmpty(lastNameText.textProperty()));
        buttonRemove.disableProperty().bind(noPersonSelected);
        buttonEdit.disableProperty().bind(noPersonSelected);

    }
    
    public void initData(ObservableList data) {
        this.data = data;
        listView.setItems(data);
        listView.setCellFactory(c -> new PersonListCell());
    }
    
    public ObservableList getPersons() {
        return data;
    }
    
    @FXML
    private void onAddItem(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(PersonManagerApp.class.getResource("view/PersonDetailsView.fxml"));
        try {
            VBox root = (VBox) loader.load();
            PersonDetailsViewController controller = loader.<PersonDetailsViewController>getController();
            controller.initPersona(null, -1);
            controller.initData(data);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("New Person");
            stage.setResizable(false);
            controller.initStage(stage);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PersonManagerViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onEditItem(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(PersonManagerApp.class.getResource("view/PersonDetailsView.fxml"));
        try {
            VBox root = (VBox) loader.load();
            PersonDetailsViewController controller = loader.<PersonDetailsViewController>getController();
            controller.initPersona(listView.getSelectionModel().getSelectedItem(), listView.getSelectionModel().getSelectedIndex());
            controller.initData(data);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("New Person");
            stage.setResizable(false);
            controller.initStage(stage);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PersonManagerViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onRemoveItem(ActionEvent event) {
        data.remove(listView.getSelectionModel().getSelectedIndex());
    }

}
