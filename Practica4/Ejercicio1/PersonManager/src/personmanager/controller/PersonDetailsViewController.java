/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personmanager.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import personmanager.PersonManagerApp;
import personmanager.model.Person;

/**
 * FXML Controller class
 *
 * @author Cotel
 */
public class PersonDetailsViewController implements Initializable {

    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
    @FXML
    private Button buttonOk;
    @FXML
    private Button buttonCancel;
    
    private int editPos;
    private PersonManagerViewController mainController;
    private Stage thisStage;
    private ObservableList<Person> data;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        FXMLLoader loader = new FXMLLoader(PersonManagerApp.class.getResource("view/PersonManagerView.fxml"));
        try{
            loader.load();
            this.mainController = loader.<PersonManagerViewController>getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void initData(ObservableList data) {
        this.data = data;
    }
    
    public void initStage(Stage thisStage) {
        this.thisStage = thisStage;
    }

    public void initPersona(Person p, int editPos) {
        if(p != null) {
            firstName.setText(p.getFirstName());
            lastName.setText(p.getLastName());
            this.editPos = editPos;
        } else {
            this.editPos = -1;
        }
    }
    
    @FXML
    public void handleOk() {
        Person nP = new Person(firstName.getText(), lastName.getText());
        if(editPos == -1) {
            data.add(nP);
        } else {
            data.set(editPos, nP);
        }
        thisStage.hide();
        System.out.println(data.size());
        mainController.initData(data);
        
    }
    
    @FXML
    public void handleCancel() {
        thisStage.hide();
    }
    
}
