/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc;

import entregableipc.controllers.MainViewController;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Cotel
 */
public class MainApp extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Locale locale = Locale.getDefault();
        try {
            File file = new File("EntregableIPC.config");
            Scanner scanner = new Scanner(file);
            String lectura = scanner.nextLine();
            if(lectura.equals("English")) {
                locale = Locale.UK;
            } else if(lectura.equals("Español")) {
                locale = new Locale("es", "ES");
            }
        } catch (IOException e) {
            e.printStackTrace();
            locale = Locale.getDefault();
        }
        
        ResourceBundle bundle = ResourceBundle.getBundle("entregableipc.controllers.resources.strings", locale);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/MainView.fxml"), bundle);
        Parent root = loader.load();
        MainViewController controller = loader.<MainViewController>getController();
        
        controller.setStage(stage);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Generador presupuestos");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
