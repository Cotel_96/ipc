/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Cotel
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListaComponentes {
    
    @XmlElement
    private List<Producto> lista = null;
    
    public ListaComponentes() {
        this.lista = new ArrayList<>();
    }
    
    public List<Producto> getLista() {
        return this.lista;
    }
    
    public void setLista(List<Producto> lista) {
        this.lista = lista;
    }
    
    @Override
    public String toString() {
        String res = "";
        for (Producto producto : lista) {
            res += producto.getDescripcion() + " x" + producto.getCantidad() + ", ";
        }
        return res;
    }
    
}
