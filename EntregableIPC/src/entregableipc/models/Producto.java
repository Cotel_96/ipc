/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.models;

import es.upv.inf.Product;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Cotel
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso(Product.class)
public class Producto{
    private StringProperty descripcion;
    private DoubleProperty precio;
    private IntegerProperty stock;
    private BooleanProperty isInStock;
    private IntegerProperty cantidad;
    
    private Product.Category categoria;
    private Product original;
    
    public Producto() {
        this.descripcion = new SimpleStringProperty();
        this.precio = new SimpleDoubleProperty();
        this.stock = new SimpleIntegerProperty();
        this.isInStock = new SimpleBooleanProperty();
        this.cantidad = new SimpleIntegerProperty();
        this.categoria = null;
        this.original = null;
        
    }
    
    public Producto(Product p) {
        this.original = p;
        this.categoria = p.getCategory();
        this.descripcion = new SimpleStringProperty(p.getDescription());
        this.precio = new SimpleDoubleProperty(p.getPrice());
        this.stock = new SimpleIntegerProperty(p.getStock());
        this.isInStock = new SimpleBooleanProperty(this.precio.getValue() > 0);
        this.cantidad = new SimpleIntegerProperty(0);
    }
    
    @XmlElement
    public int getCantidad() {
        return this.cantidad.get();
    }
    
    public IntegerProperty cantidadProperty() {
        return this.cantidad;
    }
    
    public void incCantidad() {
        this.cantidad.set(this.cantidad.get()+1);
    }
    
    public void decCantidad() {
        this.cantidad.set(this.cantidad.get()-1);
    }
    
    @XmlElement
    public Product.Category getCategoria() {
        return this.categoria;
    }
    
    public void setCategoria(Product.Category c) {
        this.categoria = c;
    }
    
    @XmlTransient
    public Product getOriginal() {
        return this.original;
    }
    
    @XmlElement
    public boolean getIsInStock() {
        return this.isInStock.get();
    }
    
    @XmlElement
    public String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
    
    @XmlElement
    public double getPrecio() {
        return precio.get();
    }

    public void setPrecio(double precio) {
        this.precio .set(precio);
    }
    
    @XmlElement
    public int getStock() {
        return stock.get();
    }

    public void setStock(int stock) {
        this.stock.set(stock);
    }

    public void setIsInStock(boolean isInStock) {
        this.isInStock.set(isInStock);
    }

    public void setCantidad(int cantidad) {
        this.cantidad.set(cantidad);
    }

    public void setOriginal(Product original) {
        this.original = original;
    }
    
    @Override
    public boolean equals(Object p) {
        if(p instanceof Producto) {
            Producto producto = (Producto) p;
            if((producto.descripcion.get().equals(this.descripcion.get())) && 
                    (producto.precio.get() == this.precio.get()) &&
                    (producto.stock.get() == this.stock.get())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
