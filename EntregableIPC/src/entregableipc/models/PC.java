/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.models;

import es.upv.inf.Product;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Cotel
 */
@XmlRootElement
@XmlType(propOrder = {"nombrePC", "componentes", "precioPC"})
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({Producto.class, ListaComponentes.class})
public class PC{
    
    private Map<Product.Category, ListaComponentes> componentes;
    private StringProperty nombrePC;
    private DoubleProperty precioPC;
    
    public PC(Map comps, String nombre) {
        this.componentes = comps;
        this.nombrePC = new SimpleStringProperty(nombre);
        this.precioPC = this.calculatePrecioPC();
    }
    
    public PC() {
        this.componentes = new HashMap<>();
        this.nombrePC = new SimpleStringProperty("nuevo PC");
        this.precioPC = new SimpleDoubleProperty(0.0);
    }
    
    public boolean isValid() {
        return (!componentes.isEmpty()) &&
                componentes.containsKey(Product.Category.MOTHERBOARD) &&
               componentes.containsKey(Product.Category.CPU) &&
               componentes.containsKey(Product.Category.RAM) &&
               componentes.containsKey(Product.Category.GPU) &&
               (componentes.containsKey(Product.Category.HDD) ||
                componentes.containsKey(Product.Category.HDD_SSD)) &&
               componentes.containsKey(Product.Category.CASE);
    }
    
    public ArrayList componentesFaltantes() {
        ArrayList<Product.Category> res = new ArrayList<>();
        res.add(Product.Category.MOTHERBOARD);
        res.add(Product.Category.CPU);
        res.add(Product.Category.GPU);
        res.add(Product.Category.RAM);
        res.add(Product.Category.HDD);
        res.add(Product.Category.CASE);        
        for (Product.Category re : componentes.keySet()) {
            if(res.contains(re)){
                res.remove(re);
            }
        }
        return res;
    }
    
    public List getListaComponentes() {
        List<Producto> productos = new ArrayList<>();
        for (ListaComponentes listaPs : componentes.values()) {
            for (int i=0; i<listaPs.getLista().size(); i++) {
                productos.add(listaPs.getLista().get(i));
            }
        }
        return productos;
    }
    
    public DoubleProperty calculatePrecioPC() {
        double res = 0.0;
        for (Map.Entry<Product.Category, ListaComponentes> category : componentes.entrySet()) {
            for (int i=0; i<category.getValue().getLista().size(); i++) {
                Producto producto = category.getValue().getLista().get(i);
                res += producto.getPrecio() * producto.getCantidad();
            }
        }
        this.precioPC.set(res);
        return this.precioPC;
    }
    
    public void addComponente(Product.Category c, Producto p) {
        if(componentes.containsKey(c)) {
            if(!componentes.get(c).getLista().contains(p))
                componentes.get(c).getLista().add(p);
            componentes.get(c).getLista().get(componentes.get(c).getLista().indexOf(p)).incCantidad();
        } else {
            ArrayList<Producto> aux = new ArrayList<Producto>();
            p.incCantidad();
            aux.add(p);
            ListaComponentes listaC = new ListaComponentes();
            listaC.setLista(aux);
            componentes.put(c, listaC);
        }
        setPrecioPC(calculatePrecioPC().get());
    }
    
    public boolean removeComponent(Producto p) {
        if(componentes.containsKey(p.getCategoria())) {
            List<Producto> category = componentes.get(p.getCategoria()).getLista();
            if(category.contains(p)) {
                category.get(category.indexOf(p)).decCantidad();
                if(category.get(category.indexOf(p)).getCantidad() <= 0) {
                    category.remove(p);
                }
                ListaComponentes aux = new ListaComponentes();
                aux.setLista(category);
                componentes.replace(p.getCategoria(), aux);
                
                Iterator it = componentes.entrySet().iterator();
                while(it.hasNext()) {
                    Map.Entry<Product.Category, ListaComponentes> entry = (Map.Entry) it.next();
                    if(entry.getValue().getLista().isEmpty() || entry.getValue() == null) {
                        it.remove();
                    }
                }
                
                setPrecioPC(calculatePrecioPC().get());
                return true;
            } else {
                return false;
            }            
        } else {
            return false;
        }
    }
    
    @XmlElement(name="precio_PC")
    public double getPrecioPC() {
        return this.precioPC.get();
    }
    
    public void setPrecioPC(double dp) {
        this.precioPC.set(dp);
    }
    
    @XmlTransient
    public DoubleProperty getPrecioProperty() {
        return this.precioPC;
    }
    
    public String getNombrePC() {
        return this.nombrePC.get();
    }
    
    @XmlElement(name="nombre_PC")
    public void setNombrePC(String nNombre) {
        this.nombrePC.set(nNombre);
    }
    
    @XmlElement(name="componentes")
    public Map getComponentes() {
        return this.componentes;
    }
    
    public void setComponentes(Map comps) {
        this.componentes = comps;
    }
    
}
