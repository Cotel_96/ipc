/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.controllers;

import entregableipc.models.PC;
import entregableipc.models.Producto;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Cotel
 */
public class ResumenViewController implements Initializable {

    @FXML
    private ImageView imagen;
    @FXML
    private TextArea resumen;
    @FXML
    private Label fechaResumen;
    @FXML
    private Button btnImprimir;
    @FXML
    private Button btnOk;
    
    private PC pc;
    private ResourceBundle bundle;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bundle = rb;
    }

    public void setPC(PC pc) {
        Image image = new Image(this.getClass().getResourceAsStream("resources/LogoTr.png"));
        imagen.setImage(image);
        this.pc = pc;
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String time = sdf.format(date);        
        fechaResumen.setText(bundle.getString("label.GeneratedIn") + " " + time);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date); cal.add(Calendar.DAY_OF_MONTH, 7);
        String timeLimit = sdf.format(cal.getTime());
        
        
        
        String res = "";
        List<Producto> productos = pc.getListaComponentes();
        
        res += "LOGICAL COMPONENTS\n";
        res += bundle.getString("label.GeneratedIn") + " " + time + ". " + bundle.getString("label.ValidUntil") + " " + timeLimit + "\n";
        res += "PC: " + pc.getNombrePC() + "\n\n";        
        
        String formato = "%-50s\t%8.2f€ x%02d\t= %5.2f€\n\n";
        for (Producto producto : productos) {
            res += ProductoListCell.evaluarCategoria(producto.getCategoria()) + "\n";            
            res += String.format(formato,
                    producto.getDescripcion(),                                        
                    producto.getPrecio(),
                    producto.getCantidad(),
                    producto.getPrecio() * producto.getCantidad()
                    );
        }
        
        res += String.format("%-30s %8.2f€\n", bundle.getString("label.SubtotalNoIVA"), pc.getPrecioPC());
        res += String.format("%-30s %8.2f€\n", bundle.getString("label.IVA"), (pc.getPrecioPC()*0.21));
        res += String.format("%-30s %8.2f€\n", bundle.getString("label.CalculatedPrice"), ((pc.getPrecioPC()*0.21) + pc.getPrecioPC()));
        
        resumen.setText(res);
    }
    
    @FXML
    public void handleOk(ActionEvent event) {
        Node n = (Node) event.getSource();
        n.getScene().getWindow().hide();
    }
    
    @FXML
    public void handlePrint(ActionEvent event) {
        Node n = resumen;
        
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);
        
        n.setScaleX(0.7);
        n.setTranslateX(-150);
        n.setTranslateY(-120);
        n.setStyle("-fx-font-size:10px;");
        
        PrinterJob job = PrinterJob.createPrinterJob(printer);
        if(job != null && job.showPrintDialog(n.getScene().getWindow())) {
            boolean success = job.printPage( n);
            if(success) {
                job.endJob();
            }
        }
        
        n.setScaleX(1);
        n.setTranslateX(0);
        n.setTranslateY(0);
        n.setStyle("-fx-font-size:12px;");
        
    }
    
}
