/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.controllers;

import entregableipc.MainApp;
import entregableipc.models.PC;
import entregableipc.models.Producto;
import es.upv.inf.Database;
import es.upv.inf.Product;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import jdk.nashorn.internal.parser.TokenType;

/**
 *
 * @author Cotel
 */
public class MainViewController implements Initializable {
    
    @FXML
    private TabPane presupuestos;
    @FXML
    private Tab mainTab;
    @FXML
    private VBox mainContent;
    @FXML
    private MenuItem nuevoPresupuesto;
    @FXML
    private MenuItem abrirPresupuesto;
    @FXML
    private MenuItem guardarPresupuesto;
    @FXML
    private MenuItem generarResumen;
    @FXML
    private ListView<String> categorias;
    @FXML
    private TableView<Producto> productos;
    @FXML
    private TableColumn<Producto, String> nombreProducto;
    @FXML
    private TableColumn<Producto, Integer> stockProducto;
    @FXML
    private TableColumn<Producto, Double> precioProducto;
    @FXML
    private TableColumn<Producto, Integer> columnaBotones;
    @FXML
    private ListView<Producto> carroCompra;
    @FXML
    private Label precioCarro;
    @FXML
    private TextField inText;
    @FXML
    private TextField minPrice;
    @FXML
    private TextField maxPrice;
    @FXML
    private CheckBox stockCheckbox;
    @FXML
    private Button filterButton;
    
    private ObservableList<String> listaCategorias;
    private List<PC> listaPresupuestos;
    private Stage window;
    private int presupuestoActual;
    private ResourceBundle bundle;
    
    public MainViewController() {        
        listaPresupuestos = new ArrayList<>();        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bundle = rb;
        PC startPC = new PC();        
        listaPresupuestos.add(startPC);
        
        ArrayList<String> categoriasAux = new ArrayList<String>();
        categoriasAux.add(bundle.getString("category.Motherboard")+"*");
        categoriasAux.add(bundle.getString("category.CPU")+"*");
        categoriasAux.add(bundle.getString("category.RAM")+"*");
        categoriasAux.add(bundle.getString("category.GPU")+"*");
        categoriasAux.add(bundle.getString("category.HDD")+"*");
        categoriasAux.add(bundle.getString("category.SDD"));
        categoriasAux.add(bundle.getString("category.Case")+"*");
        categoriasAux.add(bundle.getString("category.Keyboard"));
        categoriasAux.add(bundle.getString("category.Mouse"));
        categoriasAux.add(bundle.getString("category.Screen"));
        categoriasAux.add(bundle.getString("category.Speakers"));
        categoriasAux.add(bundle.getString("category.Multireader"));
        categoriasAux.add(bundle.getString("category.DVDWriter"));
        categoriasAux.add(bundle.getString("category.Fan"));
        categoriasAux.add(bundle.getString("category.PowerSupply"));        
        listaCategorias = FXCollections.observableList(categoriasAux);
        
        categorias.setItems(listaCategorias);
        nombreProducto.setCellValueFactory(new PropertyValueFactory<Producto, String>("descripcion"));
        nombreProducto.setStyle("-fx-alignment: CENTER-LEFT");
        stockProducto.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("stock"));
        stockProducto.setStyle("-fx-alignment: CENTER");
        precioProducto.setCellValueFactory(new PropertyValueFactory<Producto, Double>("precio"));
        precioProducto.setStyle("-fx-alignment: CENTER");
        
        presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
        carroCompra.setCellFactory(c -> new ProductoListCell(listaPresupuestos.get(presupuestoActual), carroCompra, bundle));
        
        mainTab.setOnCloseRequest(this::handleCloseTab);
        
        presupuestos.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                carroCompra.setCellFactory(c -> new ProductoListCell(listaPresupuestos.get(presupuestoActual), carroCompra, bundle));
                carroCompra.getItems().clear();
                changePresupuestoActual(newValue.intValue());
                List<Producto> componentesPcActual = new ArrayList<>(listaPresupuestos.get(newValue.intValue()).getListaComponentes());
                ObservableList<Producto> pcActual = FXCollections.observableList(componentesPcActual);
                carroCompra.setItems(pcActual);                
                
                precioCarro.setText(String.format("%8.2f€", listaPresupuestos.get(newValue.intValue()).calculatePrecioPC().get()));
                listaPresupuestos.get(presupuestoActual).calculatePrecioPC().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        precioCarro.setText(String.format("%8.2f€", newValue.doubleValue()));
                    }
                });
            }
        });        
        
        columnaBotones.setCellFactory(columna -> {
            Button addButton = new Button("+");
            TableCell<Producto, Integer> cell = new TableCell<Producto, Integer>() {
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) { setGraphic(null); }
                    else { setGraphic(addButton); }
                }
            };
            
            addButton.setOnAction((ActionEvent event) -> {
                //int presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
                Producto productoCelda = productos.getItems().get(cell.getIndex());
                
                List<Producto> componentesPcActual = new ArrayList<>(listaPresupuestos.get(presupuestoActual).getListaComponentes());
                if( !componentesPcActual.contains(productoCelda) || 
                   componentesPcActual.get(componentesPcActual.indexOf(productoCelda)).getCantidad() < productoCelda.getStock()) {
                        listaPresupuestos.get(presupuestoActual).addComponente(
                                productoCelda.getCategoria(),
                                productoCelda);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(bundle.getString("alert.StockErrorHeader"));
                    alert.setContentText(bundle.getString("alert.StockErrorContent"));
                    
                    alert.showAndWait();
                }
                                
                ObservableList<Producto> listaCom = FXCollections.observableArrayList(listaPresupuestos.get(presupuestoActual).getListaComponentes());
                carroCompra.setItems(listaCom);
                precioCarro.setText(String.format("%8.2f€", listaPresupuestos.get(presupuestoActual).calculatePrecioPC().get()));
                event.consume();
                
            });
            return cell;
        });
        columnaBotones.setStyle("-fx-alignment: CENTER");
        
        categorias.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                List<Product> catProducts = Database.getProductByCategory(evaluarCategoria(newValue.intValue()));
                ObservableList<Producto> productosAMostrar = FXCollections.observableArrayList();
                for (Product producto : catProducts) {
                    productosAMostrar.add(new Producto(producto));
                }
                productos.setItems(productosAMostrar);
            }
            
        });
        
        List<Producto> componentesPcActual = new ArrayList<>(listaPresupuestos.get(presupuestos.getSelectionModel().getSelectedIndex()).getListaComponentes());
        ObservableList<Producto> pcActual = FXCollections.observableList(componentesPcActual);
        carroCompra.setItems(pcActual);
        
        precioCarro.setText(String.format("%8.2f€", listaPresupuestos.get(presupuestoActual).calculatePrecioPC().get()));
        listaPresupuestos.get(presupuestoActual).calculatePrecioPC().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                precioCarro.setText(String.format("%8.2f€", newValue.doubleValue()));
            }
        });
    }
    
    private void changePresupuestoActual(int i) {
        this.presupuestoActual = i;
        System.out.println(presupuestoActual);
    }
    
    private void handleCloseTab(Event event) {
        if(listaPresupuestos.size() > 1) {
            if(!listaPresupuestos.get(presupuestoActual).getListaComponentes().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle(bundle.getString("alert.CloseCartTitle"));
                alert.setHeaderText(bundle.getString("alert.CloseCartHeader"));
                alert.setContentText(bundle.getString("alert.CloseCartContent"));

                ButtonType yes = new ButtonType("Yes", ButtonData.YES);
                ButtonType no = new ButtonType("No", ButtonData.NO);
                ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(yes, no, cancel);

                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == yes) {
                    guardarConfiguracion(event);
                    listaPresupuestos.remove(presupuestoActual);
                    presupuestos.getSelectionModel().selectNext();
                } else if(result.get() == no) {
                    listaPresupuestos.remove(presupuestoActual);
                    presupuestos.getSelectionModel().selectNext();
                } else if(result.get() == cancel) {
                    event.consume();
                }        
            } else {
                listaPresupuestos.remove(presupuestoActual);
                presupuestos.getSelectionModel().selectNext();
            }
        } else {
            event.consume();
        }
    }
    
    private boolean isNumeric(String s) {
        try {
            double d = Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    @FXML
    public void applyFilter(Event event) {
        boolean precioAcotado = (!minPrice.getText().equals("")) && (!maxPrice.getText().equals(""));
        boolean intextLleno = !inText.getText().equals("");
        
        boolean isNumeric = ( (isNumeric(minPrice.getText())) && (isNumeric(maxPrice.getText())) ); 
        double min=0; double max=10000;
        if(isNumeric) {
            if(minPrice.getText().equals("")) {
                min = 0;
            } else {
                min = Double.parseDouble(minPrice.getText());
            }
            if(maxPrice.getText().equals("")) {
                max = 10000;
            } else {
                max = Double.parseDouble(maxPrice.getText());
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.BadFilterHeader"));
            alert.setContentText(bundle.getString("alert.BadFilterContent"));
            alert.showAndWait();
            event.consume();
        }
        
        boolean precioCorrecto = min <= max;
        if(!precioCorrecto) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.BadFilterHeader"));
            alert.setContentText(bundle.getString("alert.BadFilterContent"));
            alert.showAndWait();
            event.consume();
        }
        
        List<Product> catProducts;
        if(precioAcotado && intextLleno) {
            catProducts = Database.getProductByCategoryDescriptionAndPrice(
                    evaluarCategoria(categorias.getSelectionModel().getSelectedIndex()),
                    inText.getText(),
                    min,
                    max,
                    stockCheckbox.isSelected());
        } else if(precioAcotado) {
            catProducts = Database.getProductByCategoryAndPrice(
                    evaluarCategoria(categorias.getSelectionModel().getSelectedIndex()),
                    min,
                    max,
                    stockCheckbox.isSelected());
        } else if(intextLleno) {
             catProducts = Database.getProductByCategoryAndDescription(
                    evaluarCategoria(categorias.getSelectionModel().getSelectedIndex()),
                    inText.getText(),
                    stockCheckbox.isSelected());
        } else {
            catProducts = Database.getProductByCategory(
                    evaluarCategoria(categorias.getSelectionModel().getSelectedIndex()));
        }
        
        ObservableList<Producto> productosAMostrar = FXCollections.observableArrayList();
        for (Product producto : catProducts) {
            productosAMostrar.add(new Producto(producto));
        }
        productos.setItems(productosAMostrar);
    }
    
    
    @FXML
    public boolean guardarConfiguracion(Event event) {
        //int presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
        PC pc = listaPresupuestos.get(presupuestoActual);
        
        if(listaPresupuestos.get(presupuestoActual).getComponentes().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.IncompleteHeader"));
            alert.setContentText(bundle.getString("alert.EmptyContent"));
            
            Optional<ButtonType> result = alert.showAndWait();
            return false;
        }
        
        if(!listaPresupuestos.get(presupuestoActual).isValid()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.IncompleteHeader"));
            String faltantes = bundle.getString("alert.IncompleteContent")+"\n";
            ArrayList<Product.Category> faltan = listaPresupuestos.get(presupuestoActual).componentesFaltantes();
            for (Product.Category category : faltan) {
                faltantes += "\t" + ProductoListCell.evaluarCategoria(category) + "\n";
            }
            alert.setContentText(faltantes);
            
            Optional<ButtonType> result = alert.showAndWait();
            return false;
        }
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(bundle.getString("fileChooser.SaveTitle"));
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter(bundle.getString("fileChooser.FilterAll"), "*.*")
        );
        fileChooser.setInitialFileName(pc.getNombrePC()+".xml");
        
        File file = fileChooser.showSaveDialog(window);
        if(file != null) {
            try {
                pc.setNombrePC(file.getName().substring(0, file.getName().lastIndexOf(".")));
                JAXBContext context = JAXBContext.newInstance(PC.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.marshal(pc, file);
                presupuestos.getSelectionModel().getSelectedItem().setText(pc.getNombrePC());
            } catch (JAXBException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(bundle.getString("alert.WriteErrorHeader"));
                alert.setContentText(bundle.getString("alert.WriteErrorContent"));
                alert.showAndWait();
            }
        } else {
            event.consume();
        }
        
        return true;
    }
    
    @FXML
    public boolean abrirConfiguracion(Event event) {
        //int presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
        if(!listaPresupuestos.get(presupuestoActual).getComponentes().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(bundle.getString("alert.SaveConfirmationTitle"));
            alert.setHeaderText(bundle.getString("alert.SaveConfirmationHeader"));
            alert.setContentText(bundle.getString("alert.SaveConfirmationContent"));
            
            ButtonType yes = new ButtonType("Yes", ButtonData.YES);
            ButtonType no = new ButtonType("No", ButtonData.NO);
            ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(yes, no, cancel);

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == yes) {
                guardarConfiguracion(event);                
            } else if(result.get() == no) {
               
            } else if(result.get() == cancel) {
                event.consume();
                return false;
            } 
            
//            Optional<ButtonType> result = alert.showAndWait();
//            if(result.get() == ButtonType.OK) {
//                guardarConfiguracion();
//            }
        }
            
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(bundle.getString("fileChooser.OpenTitle"));
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter(bundle.getString("fileChooser.FilterXML"), "*.xml")
        );
        
        File file = fileChooser.showOpenDialog(window);
        if(file != null) {
            try {
                JAXBContext context = JAXBContext.newInstance(PC.class);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                PC pc = (PC) unmarshaller.unmarshal(file);
                listaPresupuestos.set(presupuestoActual, pc);
                
                carroCompra.getItems().clear();
                carroCompra.setCellFactory(c -> new ProductoListCell(pc, carroCompra, bundle));
                ObservableList<Producto> pcActual = FXCollections.observableList(pc.getListaComponentes());
                carroCompra.setItems(pcActual);
                precioCarro.setText(Double.toString(pc.getPrecioPC())+"€");
                presupuestos.getSelectionModel().getSelectedItem().setText(pc.getNombrePC());
                
                listaPresupuestos.get(presupuestoActual).calculatePrecioPC().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        precioCarro.setText(String.format("%8.2f€", newValue.doubleValue()));
                    }
                });
            } catch (JAXBException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(bundle.getString("alert.ReadErrorHeader"));
                alert.setContentText(bundle.getString("alert.ReadErrorContent"));
                alert.showAndWait();
            }
            
        } else {
            event.consume();
        }
        return true;
    }
    
    
    
    @FXML
    public boolean cargarPreconfigurado(ActionEvent event) throws URISyntaxException {
        MenuItem clickedOption = (MenuItem) event.getSource();
        //int presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
        if(!listaPresupuestos.get(presupuestoActual).getComponentes().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(bundle.getString("alert.SaveConfirmationTitle"));
            alert.setHeaderText(bundle.getString("alert.SaveConfirmationHeader"));
            alert.setContentText(bundle.getString("alert.SaveConfirmationContent"));
            
            ButtonType yes = new ButtonType("Yes", ButtonData.YES);
            ButtonType no = new ButtonType("No", ButtonData.NO);
            ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(yes, no, cancel);

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == yes) {
                guardarConfiguracion(event);
            } else if(result.get() == no) {
                
            } else if(result.get() == cancel) {
                event.consume();
                return false;
            } 

//            Optional<ButtonType> result = alert.showAndWait();
//            if(result.get() == ButtonType.OK) {
//                guardarConfiguracion();
//            }
        }
        
        InputStream file = null;
        if(clickedOption.getId().equals("pc_oferta")) {
            file = this.getClass().getResourceAsStream("resources/PCBarato.xml");
        } else if(clickedOption.getId().equals("pc_medio")) {
            file = this.getClass().getResourceAsStream("resources/PCMedio.xml");
        } else if(clickedOption.getId().equals("pc_gaming")) {
            file = this.getClass().getResourceAsStream("resources/PCGaming.xml");
        }
        
        try {
            JAXBContext context = JAXBContext.newInstance(PC.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            PC pc = (PC) unmarshaller.unmarshal(file);
            listaPresupuestos.set(presupuestoActual, pc);

            carroCompra.getItems().clear();
            carroCompra.setCellFactory(c -> new ProductoListCell(pc, carroCompra, bundle));
            ObservableList<Producto> pcActual = FXCollections.observableList(pc.getListaComponentes());
            carroCompra.setItems(pcActual);
            precioCarro.setText(Double.toString(pc.getPrecioPC())+"€");
            presupuestos.getSelectionModel().getSelectedItem().setText(pc.getNombrePC());

            listaPresupuestos.get(presupuestoActual).calculatePrecioPC().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    precioCarro.setText(String.format("%8.2f€", newValue.doubleValue()));
                }
            });
        } catch (JAXBException ex) {
            ex.printStackTrace();
        } finally {
            event.consume();
        }
        return true;
    }
    
    @FXML
    public boolean generarResumen() {
        //int presupuestoActual = presupuestos.getSelectionModel().getSelectedIndex();
        
        if(listaPresupuestos.get(presupuestoActual).getComponentes().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.IncompleteHeader"));
            alert.setContentText(bundle.getString("alert.EmptyContent"));
            
            Optional<ButtonType> result = alert.showAndWait();
            return false;
        }
        
        if(!listaPresupuestos.get(presupuestoActual).isValid()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(bundle.getString("alert.IncompleteHeader"));
            String faltantes = bundle.getString("alert.IncompleteContent")+"\n";
            ArrayList<Product.Category> faltan = listaPresupuestos.get(presupuestoActual).componentesFaltantes();
            for (Product.Category category : faltan) {
                faltantes += "\t" + ProductoListCell.evaluarCategoria(category) + "\n";
            }
            alert.setContentText(faltantes);
            
            Optional<ButtonType> result = alert.showAndWait();
            return false;
        }
        
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("views/ResumenView.fxml"), bundle);
        try {
            AnchorPane root = (AnchorPane) loader.load();
            ResumenViewController controller = loader.<ResumenViewController>getController();
            controller.setPC(listaPresupuestos.get(presupuestoActual));
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setTitle(bundle.getString("window.Bill"));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return true;
    }
    
    @FXML
    public void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(bundle.getString("menuItem.About"));
        alert.setHeaderText("Entregable IPC");
        alert.setContentText("By Miguel Coleto Muñoz\n"+
                "https://github.com/Cotel/\n"+
                "ETSINF Grupo 2C1.\n"+                
                "2016");
        alert.showAndWait();
    }
    
    @FXML
    public void selectLanguage(Event event) {
        List<String> language = new ArrayList<>();
        language.add("English");
        language.add("Español");
        
        ChoiceDialog<String> dialog = new ChoiceDialog<>("English", language);
        dialog.setTitle(bundle.getString("dialog.SelectLanguage"));
        dialog.setHeaderText(bundle.getString("dialog.Header"));
        dialog.setContentText(bundle.getString("dialog.Content"));
        
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            try {
                File file = new File("entregableIPC.config");
                PrintWriter pw = new PrintWriter(file);            
                pw.println(result.get());            
                pw.flush();
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        } else {
            event.consume();
        }
        
    }
    
    @FXML
    public void nuevoPresupuesto() {
        PC aux = new PC();
        listaPresupuestos.add(aux);
        
        Tab newTab = new Tab(aux.getNombrePC());
        newTab.setOnCloseRequest(this::handleCloseTab);
        presupuestos.getTabs().add(newTab);
        
        
        presupuestos.getSelectionModel().select(newTab);
        carroCompra.setCellFactory(c -> new ProductoListCell(listaPresupuestos.get(presupuestoActual), carroCompra, bundle));
    }
    

    private Product.Category evaluarCategoria(int i) {
        Product.Category res = null;
        switch(i) {
            case 0:
                res = Product.Category.MOTHERBOARD;
                break;
            case 1:
                res = Product.Category.CPU;
                break;
            case 2:
                res = Product.Category.RAM;
                break;
            case 3:
                res = Product.Category.GPU;
                break;
            case 4:
                res = Product.Category.HDD;
                break;
            case 5:
                res = Product.Category.HDD_SSD;
                break;
            case 6:
                res = Product.Category.CASE;
                break;
            case 7:
                res = Product.Category.KEYBOARD;
                break;
            case 8:
                res = Product.Category.MOUSE;
                break;
            case 9:
                res = Product.Category.SCREEN;
                break;
            case 10:
                res = Product.Category.SPEAKER;
                break;
            case 11:
                res = Product.Category.MULTIREADER;
                break;
            case 12:
                res = Product.Category.DVD_WRITER;
                break;
            case 13:
                res = Product.Category.FAN;
                break;
            case 14:
                res = Product.Category.POWER_SUPPLY;
                break;
            default:
                res = null;
                break;                
        }
        return res;
    }

    public void setStage(Stage stage) {
        this.window = stage;
    }
    
}
