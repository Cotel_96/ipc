/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entregableipc.controllers;

import entregableipc.models.PC;
import entregableipc.models.Producto;
import es.upv.inf.Product;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 *
 * @author Cotel
 */
public class ProductoListCell extends ListCell<Producto>{
    
    private Button deleteButton;
    private PC actualPC;
    private ListView<Producto> carroCompra;
    private static ResourceBundle bundle;
    
    public ProductoListCell(PC pc, ListView<Producto> carro, ResourceBundle rb) {
        this.deleteButton = new Button("-");
        this.actualPC = pc;
        this.carroCompra = carro;
        this.bundle = rb;
        this.setStyle("-fx-font-family: 'Bitstream Vera Sans Mono';");
    }
    
    @Override
    protected void updateItem(Producto item, boolean empty) {        
        super.updateItem(item, empty);
        if(item == null || empty) {
            setText(null);
            setGraphic(null);
        } else {
            deleteButton.textProperty().bind(item.cantidadProperty().asString());
            deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    actualPC.removeComponent(item);
                    //carroCompra.getItems().clear();
                    ObservableList<Producto> listaNueva = FXCollections.observableArrayList(actualPC.getListaComponentes());
                    carroCompra.setItems(listaNueva);
                    //System.out.println(actualPC.hashCode() + " " +actualPC.getComponentes().toString());
                }
            });
            
            StringExpression etiq = Bindings.format("%-25s%-50s%8.2f€",
                    evaluarCategoria(item.getCategoria()),
                    item.getDescripcion(),
                    item.getPrecio());
            
            setText(etiq.get());
            setGraphic(deleteButton);
            
//            item.cantidadProperty().addListener(new ChangeListener<Number>() {
//                @Override
//                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//                    setText("x"+item.getCantidad() + "\t\t" + evaluarCategoria(item.getCategoria()) + "\t\t" + item.getDescripcion() + "\t\t" + item.getPrecio() + "€");
//                }
//            });
            
        }
    }
    
    public static String evaluarCategoria(Product.Category category) {
        String res = "";
        switch(category) {
            case MOTHERBOARD:
                res = bundle.getString("category.Motherboard");
                break;
            case CPU:
                res = bundle.getString("category.CPU");
                break;
            case RAM:
                res = bundle.getString("category.RAM");
                break;
            case GPU:
                res = bundle.getString("category.GPU");
                break;
            case HDD:
                res = bundle.getString("category.HDD");
                break;
            case HDD_SSD:
                res = bundle.getString("category.SDD");
                break;
            case CASE:
                res = bundle.getString("category.Case");
                break;
            case KEYBOARD:
                res = bundle.getString("category.Keyboard");
                break;
            case MOUSE:
                res = bundle.getString("category.Mouse");
                break;
            case SCREEN:
                res = bundle.getString("category.Screen");
                break;
            case SPEAKER:
                res = bundle.getString("category.Speakers");
                break;
            case MULTIREADER:
                res = bundle.getString("category.Multireader");
                break;
            case DVD_WRITER:
                res = bundle.getString("category.DVDWriter");
                break;
            case FAN:
                res = bundle.getString("category.Fan");
                break;
            case POWER_SUPPLY:
                res = bundle.getString("category.PowerSupply");
                break;
            default:
                res = null;
                break;                
        }
        return res;
    }
}
