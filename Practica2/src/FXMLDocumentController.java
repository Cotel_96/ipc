/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author micomuo
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField output;
    @FXML
    private Button C;
    @FXML
    private Button decimales;
    @FXML
    private Button division;
    @FXML
    private Button multiplicacion;
    @FXML
    private Button igual;
    @FXML
    private Button resta;
    @FXML
    private Button suma;
    @FXML
    private Button cero;
    @FXML
    private Button uno;
    @FXML
    private Button dos;
    @FXML
    private Button tres;
    @FXML
    private Button cuatro;
    @FXML
    private Button cinco;
    @FXML
    private Button seis;
    @FXML
    private Button siete;
    @FXML
    private Button ocho;
    @FXML
    private Button nueve;
    
    @FXML
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void handleButtonPressed(ActionEvent event) throws ScriptException {
        Button clickedButton = (Button) event.getSource();
        if(clickedButton.getText().equals("C")) {
            output.setText("");
        } else if(clickedButton.getText().equals("=")) {
            if(!output.getText().equals("")) {
                ScriptEngineManager manager = new ScriptEngineManager();
                ScriptEngine engine = manager.getEngineByName("js");
                Object result = engine.eval(output.getText());
                output.setText(result.toString());
            }
        } else {
            output.setText(output.getText()+clickedButton.getText());
        }
    }
    
}
