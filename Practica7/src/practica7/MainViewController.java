/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica7;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

/**
 *
 * @author micomuo
 */
public class MainViewController implements Initializable {    

    @FXML
    private LineChart<String, Number> lineChart;
    @FXML
    private NumberAxis yLineAxis;
    @FXML
    private CategoryAxis xLineAxis;
    @FXML
    private BarChart<String, Number> barChart;
    @FXML
    private NumberAxis yBarAxis;
    @FXML
    private CategoryAxis xBarAxis;
    @FXML
    private PieChart pieChart;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Integer hist[] = new Integer[10];
        
        for(int i=0; i<hist.length; i++) {
            hist[i] = 0;
        }
        
        for(int j=0; j<1000; j++) {
            double value = Math.random()*10;
            for(int i=0; i<hist.length; i++) {
                if(i<=value && value < i+1) {
                    hist[i]++;
                    break;
                }
            }
        }
        
        List<Integer> aux = Arrays.asList(hist);
        ObservableList<PieChart.Data> list = FXCollections.observableArrayList();
        XYChart.Series series = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        int i=0;
        for (Integer n : aux) {
            list.add(new PieChart.Data(i+"-"+(i+1), n));
            series.getData().add(new XYChart.Data(""+i++, n));
            series2.getData().add(new XYChart.Data(""+i++, n));
        }
        
        series.setName("Random data");
        series2.setName("Random data");
        
        xLineAxis.setLabel("x");
        yLineAxis.setLabel("hist[x]");
        lineChart.setTitle("Random");
        lineChart.getData().add(series);
        
        xBarAxis.setLabel("x");
        yBarAxis.setLabel("hist[x]");
        barChart.setTitle("Random");
        barChart.getData().add(series2);
        
        pieChart.setTitle("Random");
        pieChart.setData(list);
        
    }    
    
}
