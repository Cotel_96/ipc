/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica5;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Cotel
 */
public class Practica5 extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Locale locale = Locale.UK;
        ResourceBundle bundle = ResourceBundle.getBundle("practica5.strings", locale);
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"), bundle);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Practica 5 IPC");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
