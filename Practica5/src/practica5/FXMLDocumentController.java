/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica5;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;

/**
 *
 * @author Cotel
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private MenuItem closeItem;
    @FXML
    private MenuItem amazonItem;
    @FXML
    private MenuItem bloggerItem;
    @FXML
    private MenuItem ebayItem;
    @FXML
    private MenuItem fbItem;
    @FXML
    private MenuItem googleItem;
    @FXML
    private RadioMenuItem amazonOption;
    @FXML
    private RadioMenuItem ebayOption;
    @FXML
    private Button amazonButton;
    @FXML
    private Button bloggerButton;
    @FXML
    private Button ebayButton;
    @FXML
    private Button fbButton;
    @FXML
    private Button googleButton;
    @FXML
    private WebView webView;
    @FXML
    private Label labelInfo;
    
    private ResourceBundle bundle;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bundle = rb;
        
        amazonOption.selectedProperty().set(true);
        ImageView amazon = new ImageView("http://icons.iconarchive.com/icons/limav/flat-gradient-social/512/Amazon-icon.png");
        amazon.setFitHeight(75); amazon.setFitWidth(75); amazon.preserveRatioProperty(); amazon.setSmooth(true);
        amazonButton.setGraphic(amazon);
        
        ImageView blogger = new ImageView("https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Blogger_icon.svg/768px-Blogger_icon.svg.png");
        blogger.setFitHeight(75); blogger.setFitWidth(75); blogger.preserveRatioProperty(); blogger.setSmooth(true);
        bloggerButton.setGraphic(blogger);
        
        ImageView ebay = new ImageView("https://d13yacurqjgara.cloudfront.net/users/1857/screenshots/729847/ebay-revision-01.png");
        ebay.setFitHeight(75); ebay.setFitWidth(75); ebay.preserveRatioProperty(); ebay.setSmooth(true);
        ebayButton.setGraphic(ebay);
        
        ImageView fb = new ImageView("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/F_icon.svg/2000px-F_icon.svg.png");
        fb.setFitHeight(75); fb.setFitWidth(75); fb.preserveRatioProperty(); fb.setSmooth(true);
        fbButton.setGraphic(fb);        
        
        ImageView google = new ImageView("http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png");
        google.setFitHeight(75); google.setFitWidth(75); google.preserveRatioProperty(); google.setSmooth(true);
        googleButton.setGraphic(google);
    }    

    @FXML
    private void handleClose(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(bundle.getString("alertTitle.Close"));
        alert.setHeaderText(bundle.getString("alertHeader.Close"));
        alert.setContentText(bundle.getString("alertContent.Close"));
        
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            System.exit(0);
        }        
        
    }

    @FXML
    private void handleAmazon(ActionEvent event) {
        if(amazonOption.isSelected()) {
            webView.getEngine().load("https://www.amazon.es/");
            labelInfo.setText(bundle.getString("labelInfo.Amazon"));
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("alertTitle.Buy"));
            alert.setHeaderText(null);
            alert.setContentText(bundle.getString("alertContent.Amazon"));
            
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("alertTitle.Buy"));
            alert.setHeaderText(bundle.getString("alertHeader.AmazonFail"));
            alert.setContentText(bundle.getString("alertContent.BuyFail"));
            alert.showAndWait();
        }
    }

    @FXML
    private void handleBlogger(ActionEvent event) {
        List<String> choices = new ArrayList<>();
        choices.add("http://www.elladodelmal.com/");
        choices.add("http://www.pybonacci.org/");
        choices.add("http://www.genbetadev.com");
        
        ChoiceDialog<String> dialog = new ChoiceDialog<>("http://www.elladodelmal.com", choices);
        dialog.setTitle(bundle.getString("dialogTitle.Blog"));
        dialog.setHeaderText(bundle.getString("dialogHeader.Blog"));
        dialog.setContentText(bundle.getString("dialogContent.Blog"));
        
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            labelInfo.setText(bundle.getString("labelInfo.Blog") + result.get());
            webView.getEngine().load(result.get());
        }
    }

    @FXML
    private void handleEbay(ActionEvent event) {
        if(ebayOption.isSelected()) {
            webView.getEngine().load("https://www.ebay.es/");
            labelInfo.setText(bundle.getString("labelInfo.Ebay"));
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("alertTitle.Buy"));
            alert.setHeaderText(null);
            alert.setContentText(bundle.getString("alertContent.Ebay"));
            
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("alertTitle.Buy"));
            alert.setHeaderText(bundle.getString("alertHeader.EbayFail"));
            alert.setContentText(bundle.getString("alertContent.BuyFail"));
            alert.showAndWait();
        }
    }

    @FXML
    private void handleFacebook(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(bundle.getString("dialogTitle.Facebook"));
        dialog.setHeaderText(bundle.getString("dialogHeader.Facebook"));
        dialog.setContentText(bundle.getString("dialogContent.Facebook"));
        
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            labelInfo.setText(bundle.getString("labelInfo.Facebook") + result.get());
            webView.getEngine().load("https://www.facebook.com/");
        }        
    }

    @FXML
    private void handleGoogle(ActionEvent event) {
        labelInfo.setText(bundle.getString("labelInfo.Google"));
        webView.getEngine().load("https://plus.google.com/");
    }

    @FXML
    private void handleBuyOption(ActionEvent event) {
        CheckMenuItem orig = (CheckMenuItem) event.getSource();
        if(orig.getId().equals("amazonOption")) {
            amazonOption.selectedProperty().set(true);
            ebayOption.selectedProperty().set(false);
        } else if(orig.getId().equals("ebayOption")) {
            amazonOption.selectedProperty().set(false);
            ebayOption.selectedProperty().set(true);
        }
    }
    
}
